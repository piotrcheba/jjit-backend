const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const OfferSchema = new Schema({
    shortCompanyName: {
        type: String,
        required: true
    },
    companyWebsite: {
        type: String,
        required: true
    },
    companyType: {
        type: String,
        required: true
    },
    companySize: {
        type: String,
        required: true
    },
    companyIndustry: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    expLevel: {
        type: String,
        required: true
    },
    employmentType: {
        type: String,
        required: true
    },
    salaryFrom: {
        type: Number,
        required: true
    },
    salaryTo: {
        type: Number,
        required: true
    },
    currency: {
        type: String,
        required: true
    },
    onlineInterview: {
        type: Boolean,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Offer = mongoose.model("offers", OfferSchema);