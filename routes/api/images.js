const express = require("express");
const router = express.Router();
const cloudinary = require("cloudinary");

// Load Offer model

const Image = require("../../models/Image");

const multer = require("multer");

const storage = multer.diskStorage({
    filename: function(req, file, callback) {
        callback(null, Date.now() + file.originalname);
    }
});

const imageFilter = function(req, file, cb) {
    // accept img files only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error("Only image files are accepted!"), false);
    }

    cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: imageFilter });

router.post("/add", upload.single("image"), (req, res) => {
    cloudinary.v2.uploader.upload(req.file.path, function(err, result) {
        if (err) {
            req.json(err.message);
        }
        req.body.image = result.secure_url;
        // add image's public_id to image object
        req.body.imageId = result.public_id;

        Image.create(req.body, function(err, image) {
            if (err) {
                res.json(err.message);
                return res.redirect("/")
            }
        });
    });
});

router.get("/", (req, res) => {
    Image.find(function(err, images) {
        if (err) {
            res.json(err.message);
        } else {
            res.json(images);
        }
    });
});

module.exports = router;