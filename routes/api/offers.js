const express = require("express");
const router = express.Router();

// Load Offer model

const Offer = require("../../models/Offer");

// @route POST api/offers/add
// @desc Add offer
// @access Public
router.post("/add", (req, res) => {
    Offer.findOne({ shortCompanyName: req.body.shortCompanyName }).then(offer => {
        if (offer) {
            return res.status(400).json({ offer: "Offer already exists" });
        } else {
            const newOffer = new Offer({
                shortCompanyName: req.body.shortCompanyName,
                companyWebsite: req.body.companyWebsite,
                companyType: req.body.companyType,
                companySize: req.body.companySize,
                companyIndustry: req.body.companyIndustry,
                title: req.body.title,
                expLevel: req.body.expLevel,
                employmentType: req.body.employmentType,
                salaryFrom: req.body.salaryFrom,
                salaryTo: req.body.salaryTo,
                currency: req.body.currency,
                onlineInterview: req.body.onlineInterview,
            });

            newOffer.save()
                .then(offer => res.json(offer))
                .catch(err => console.log(err));
        }
    })
})

module.exports = router;